from aiohttp import web

from ner import views


def setup_routes(app: web.Application):
    app.add_routes([

        web.get('/api/healthcheck', views.healthcheck, name='healthcheck',
                allow_head=False),

        web.view('/api/images/numbers/recognition',
                 views.NumberRecognitionView,
                 name='number_recognition'),

        web.get('', views.free_drawing, name='free_drawing'),

    ])
