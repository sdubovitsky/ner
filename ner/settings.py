LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': u'%(asctime)s:%(levelname)s:%(name)s.%(funcName)s: '
                      u'%(message)s',
        },
    },
    'handlers': {
        'basic': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'level': 'DEBUG',
        },
    },
    'loggers': {
        '': {
            'handlers': ['basic'],
            'level': 'DEBUG',
        },
        'aiohttp': {
            'handlers': ['basic'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'ner': {
            'handlers': ['basic'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}
