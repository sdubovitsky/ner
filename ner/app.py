import os
import logging.config

from aiohttp import ClientSession
from aiohttp import web

import aiohttp_jinja2
import jinja2

from ner.clients import TensorFlowClient
from ner.routes import setup_routes
from ner.settings import LOGGING
from ner.middlewares import envelope_middleware

logger = logging.getLogger(__name__)


async def init_http_client(app):
    app['http_client'] = ClientSession()


async def close_http_client(app):
    await app['http_client'].close()


async def init_tensorflow_client(app):
    app['tensorflow_client'] = TensorFlowClient(
        app['http_client'],
        os.getenv('TENSORFLOW_API_URL'),
    )


def init_app():
    app = web.Application(middlewares=[
        envelope_middleware
    ])
    setup_routes(app)
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))
    app.on_startup.extend([
        init_http_client,
        init_tensorflow_client,
    ])
    app.on_cleanup.extend([
        close_http_client,
    ])
    return app


if __name__ == '__main__':
    web_app = init_app()
    logging.config.dictConfig(LOGGING)
    logger.info('Starting API NER service')
    web.run_app(web_app, port=8000)
