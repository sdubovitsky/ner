

async def test_num_recognitions(client):
    with open('ner/tests/data/num5.base64', 'r') as f:
        resp = await client.post('/api/images/numbers/recognition', json={
            'image': f.read()
        })
        assert resp.status == 200
        resp_json = await resp.json()
        assert resp_json['result'] == 5
