import pytest
from ner.app import init_app


@pytest.fixture
def app(loop):
    return init_app()


@pytest.fixture
async def client(aiohttp_client, app):
    return await aiohttp_client(app)
