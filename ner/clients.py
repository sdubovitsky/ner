import logging

logger = logging.getLogger(__name__)


class TensorFlowClient:

    def __init__(self, http_client, base_url):
        self.http_client = http_client
        self.base_url = base_url

    async def number_predict(self, data: dict) -> dict:
        url = self.base_url + '/v1/models/numbers:predict'
        logger.debug('Number data: %s', data)
        async with self.http_client.post(url, json=data) as response:
            resp_json = await response.json()
            logger.debug('Number predict response %s', resp_json)
            return resp_json
