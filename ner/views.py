import base64
from io import BytesIO

import aiohttp_jinja2
from PIL import Image
from aiohttp import web
from tensorflow.keras.preprocessing import image
from ner.clients import TensorFlowClient


async def healthcheck(request):
    return {}, 200


@aiohttp_jinja2.template('free_drawing.jinja2')
async def free_drawing(self):
    return {}


class NumberRecognitionView(web.View):

    def __init__(self, request, *args, **kwargs):
        super().__init__(request, *args, **kwargs)
        self.tf_client: TensorFlowClient = self.request.app['tensorflow_client']

    async def post(self):
        data = await self.request.json()
        png_base64 = base64.b64decode(data['image'].split('base64,')[1])
        png_img = Image.open(BytesIO(png_base64))
        png_img = png_img.convert('L')
        png_img = png_img.resize((28, 28), Image.NEAREST)
        png_image_array = image.img_to_array(png_img)
        x_image = png_image_array.reshape(1, 784)
        res = await self.tf_client.number_predict({
            'instances': x_image.tolist()
        })
        predictions = res['predictions'][0]
        return {'result': predictions.index(max(predictions))}, 200
