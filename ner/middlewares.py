from aiohttp import web


@web.middleware
async def envelope_middleware(request: web.Request,
                              handler,
                              ) -> web.Response:
    response = await handler(request)
    if isinstance(response, tuple):
        data, status = response
        response = web.json_response(data=data, status=status)
    return response
