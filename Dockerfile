#FROM python:3.8.0-slim
FROM tensorflow/tensorflow

ADD ./ner/requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt  --no-cache-dir

ADD ./ner /app/ner/
WORKDIR /app

EXPOSE 8000
